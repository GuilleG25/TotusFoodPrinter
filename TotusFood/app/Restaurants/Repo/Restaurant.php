<?php

namespace App\Restaurants\Repo;

use App\Helper;
use App\Restaurants\Model\Restaurants;

class Restaurant
{
    public function store($data)
    {

        $restaurant = new Restaurants();

        $details = Helper::array_object($data['details']);
        $details = Helper::object_json($details);
        $remember = $data['remember'] == true ? true : false;

        $restaurant->id = $data['id'];
        $restaurant->name = $data['name'];
        $restaurant->owner = $data['owner'];
        $restaurant->country = $data['country'];
        $restaurant->token = $data['token'];
        $restaurant->details = $details;
        $restaurant->remember = $remember;
        $restaurant->current = true;

        $restaurant->save();
    }

    public function all($id)
    {
        $restaurants = Restaurants::find($id);
        return $restaurants;
    }

    public function refreshToken($id, $token, $remember)
    {
        $restaurant = new Restaurants();
        $remember = $remember == true ? true : false;
        $restaurant->where('id', $id)->update(['token' => $token, 'current' => true, 'remember' => $remember]);
    }

    public function logoutRestaurant($id)
    {
        Restaurants::where('id', $id)->update(['token' => '', 'current' => false, 'remember' => false]);
    }
}