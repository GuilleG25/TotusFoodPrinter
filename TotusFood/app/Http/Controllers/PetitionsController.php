<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Petitions\Model\Petitions;
use App\Petitions\Repo\PetitionsRepo;
use Illuminate\Http\Request;

class PetitionsController extends Controller
{
    private $petitionRepo;
    private $petitionModel;
    function __construct(Petitions $petitionModel, PetitionsRepo $petitionsRepo)
    {
        $this->petitionModel = $petitionModel;
        $this->petitionRepo  = $petitionsRepo;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $data = $request->get('data');
        foreach ($data as $petition) {

            $contours = ! is_null($petition['contours']) ? Helper::json_object($petition['contours'])  : [];
            $replaceables = ! is_null($petition['replaceables']) ?  Helper::json_object($petition['replaceables']) : [];
            $ingredients = ! is_null($petition['ingredients']) ?  [] : [];
            $details = Helper::json_object($petition['details']);

            $details->contours = $contours;
            $details->replaceables = $replaceables;
            $details->ingredients = $ingredients;
            $details->observation = $petition['observation'];
            $petition['details'] = Helper::object_json($details);

            $petitions = new Petitions();
            $petition_ = $this->petitionModel->where('petition_id', $petition['petition_id'])->get();
            if(count($petition_) > 0) {
                $this->petitionRepo->update($petition);
            } else {
                if(isset($petition) && count($petition) > 0) {
                    $petitions->petition_id   = $petition['petition_id'];
                    $petitions->order_id      = $petition['order_id'];
                    $petitions->type          = $petition['typeproduct'];
                    $petitions->product       = $petition['product'];
                    $petitions->status        = $petition['status'];
                    $petitions->amount        = $petition['amount'];
                    $petitions->details       = $petition['details'];
                    $petitions->account_id    = $petition['account_id'];
                    $petitions->restaurant_id = session('restaurant.id');
                    $petitions->save();
                }
            }
        }

    }

    public function change_order_account(Request $request)
    {
        if($request->has('data') && count($request->get('data')) > 0) {
            $this->delete_petitions($request->get('data'));
            $this->update_petitions($request->get('data'));
        }
    }

    private function delete_petitions($petitions)
    {
        if(isset($petitions) && count($petitions) > 0) {
            foreach ($petitions as $petition) {
                Petitions::where('account_id', (int)$petition['old_account_id'])->where('order_id', (int)$petition['old_order_id'])->where('restaurant_id', session()->get('restaurant.id'))->delete();
            }
        }
    }

    private function update_petitions($data)
    {
        foreach ($data as $petition) {

            $contours = ! is_null($petition['contours']) ? Helper::json_object($petition['contours'])  : [];
            $replaceables = ! is_null($petition['replaceables']) ?  Helper::json_object($petition['replaceables']) : [];
            $ingredients = ! is_null($petition['ingredients']) ?  Helper::json_object($petition['ingredients']) : [];
            $details = Helper::json_object($petition['details']);

            $details->contours = $contours;
            $details->replaceables = $replaceables;
            $details->ingredients = $ingredients;
            $details->observation = $petition['observation'];
            $petition['details'] = Helper::object_json($details);

            $petitions = new Petitions();
            $petition_ = $this->petitionModel->where('petition_id', $petition['petition_id'])->get();
            if(count($petition_) > 0) {
                $this->petitionRepo->update($petition);
            } else {
                if(isset($petition) && count($petition) > 0) {
                    $petitions->petition_id   = $petition['petition_id'];
                    $petitions->order_id      = (int)$petition['order_id'];
                    $petitions->type          = $petition['typeproduct'];
                    $petitions->product       = $petition['product'];
                    $petitions->status        = $petition['status'];
                    $petitions->amount        = $petition['amount'];
                    $petitions->details       = $petition['details'];
                    $petitions->account_id    = (int)$petition['account_id'];
                    $petitions->restaurant_id = session('restaurant.id');
                    $petitions->save();
                }
            }
        }
    }
}
