<?php

namespace App\Http\Controllers;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
use Illuminate\Http\Request;

class SocketIO extends Controller
{
    public static function emit($nsp, array $data)
    {
        \Log::debug('server socket ', [
            'server' => env('SERVER_SOCKET').':'.env('SERVER_SOCKET_PORT')]);
        $client = new Client(new Version1X(env('SERVER_SOCKET').':'.env('SERVER_SOCKET_PORT')));
        $client->initialize();
        $client->emit('TotusFood::server buzz', [
            'nsp' => $nsp,
            'data' => $data,
        ]);
        $client->close();
        return true;
    }

    public static function listen()
    {

        $client = new Client(new Version1X(env('SERVER_SOCKET').':'.env('SERVER_SOCKET_PORT')));
        $client->initialize();
        $listen = $client->read();

        dd($listen);

       // $client->close();
        return true;
    }
}
