<?php

namespace App\Http\Controllers;

use App\Accounts\Enums\AccountStatusEnum;
use App\Accounts\Model\Account;
use App\Accounts\Repo\AccountRepo;
use App\Helper;
use App\Petitions\Repo\FormatData;
use App\Restaurants\Model\Restaurants;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    private $formatData;
    private $accountRepo;

    function __construct(FormatData $formatData, AccountRepo $accountRepo)
    {
        $this->formatData = $formatData;
        $this->accountRepo = $accountRepo;
    }


    /**
     * Show the form for creating a new resource.
     *
     */
    public function all(Request $request)
    {
        $status = $request->get('status') == 'all' ? AccountStatusEnum::$open : $request->get('status');
        if ($status == AccountStatusEnum::$all) {
            $accounts = Account::with('petitions')->where('restaurant_id', session('restaurant.id'))->orderBy('created_at', 'desc')->get();
        } else {
            $accounts = Account::with('petitions')->where('restaurant_id', session('restaurant.id'))->where('status', $status)->orderBy('created_at', 'desc')->get();
        }
        $this->formatData->formatAccount($accounts);
        $response = [
            'status' => 200,
            'data' => $accounts,
        ];

        return $response;
    }

    public function reports($status, $restaurant)
    {
        try {
            if ($status == AccountStatusEnum::$all) {
                $accounts = Account::with('petitions')->where('restaurant_id', $restaurant)->orderBy('created_at', 'desc')->get();
            } else {
                $accounts = Account::with('petitions')->where('restaurant_id', $restaurant)->where('status', $status)->orderBy('created_at', 'desc')->get();
            }

            $restaurant = Restaurants::find($restaurant);
            $accounts = $this->formatData->formatAccount($accounts);
            foreach ($accounts as $account) {
                $account->details = Helper::object_array($account->details);
                if(isset($account->petitions) && count($account->petitions) > 0) {
                  foreach ($account->petitions as $petition) {
                      if(isset($petition->details)){
                          $details = Helper::json_object($petition->details);
                          $petition->details = Helper::object_array($details);
                      }
                  }
                }
            }

            $accounts = Helper::object_array($accounts);

            $title = $this->statusAccountTitle($status);
            $pdf = PDF::loadView('pdf.accounts', compact('accounts', 'restaurant', 'title'));
            $response = $pdf->download($title . '.pdf');

//            $response =  view('pdf.accounts', compact('accounts', 'restaurant', 'title'));

        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }

        return $response;
    }

    public function report_by_account($id, $restaurant)
    {
        try {
            $account = Account::with('petitions')->where('account_id', $id)->where('restaurant_id', $restaurant)->get();

            $restaurant = Restaurants::find($restaurant);
            $account = $this->formatData->formatAccount($account);
            foreach ($account as $items) {
                $items->details = Helper::object_array($items->details);
                if(isset($items->petitions) && count($items->petitions) > 0) {
                    foreach ($items->petitions as $petition) {
                        if(isset($petition->details)){
                            $details = Helper::json_object($petition->details);
                            $petition->details = Helper::object_array($details);
                        }
                    }
                }
            }

            $accounts = Helper::object_array($account);
            $title   = __('Reporte de la cuenta ') . '#' . $accounts[0]['token_id'];
            $pdf = PDF::loadView('pdf.accounts', compact('accounts', 'restaurant', 'title'));
            $response = $pdf->download($title . '.pdf');

//            $response =  view('pdf.accounts', compact('accounts', 'restaurant', 'title'));

        } catch (\Exception $exception) {
            $response = (object)[
                'error' => true,
                'message' => $exception->getMessage()
            ];
        }

        return $response;
    }

    private function statusAccountTitle($status)
    {
        switch ($status) {
            case AccountStatusEnum::$open:
                $status = __('Reporte de Cuentas Abiertas');
                break;
            case AccountStatusEnum::$paying:
                $status = __('Reporte de Cuentas Pagando');
                break;
            case AccountStatusEnum::$closed:
                $status = __('Reporte de Cuentas Cerradas');
                break;
            case AccountStatusEnum::$cancelled:
                $status = __('Reporte de Cuentas Canceladas');
                break;
            default:
                $status = __('Reporte de Todas las cuentas');
                break;
        }

        return $status;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function store(Request $request)
    {
        $account = $request->get('data');
        if ($request->has('data') && count($request->get('data')) > 0) {
            $exist = Account::where('account_id', $account['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->get();
            if (count($exist) <= 0) {
                $accounts = new Account();
                $accounts->account_id = $account['account_id'];
                $accounts->waiter = $account['waiter'];
                $accounts->client = $account['client'];
                $accounts->currency = Helper::object_json($account['currency']);
                $accounts->table = $account['table'];
                $accounts->status = $account['status'];
                $accounts->token_id = $account['token_id'];
                $accounts->date = $this->formatData->formatDate($account['created_at']);
                $accounts->restaurant_id = session()->get('restaurant.id');
                $accounts->save();
            } else {
                $this->accountRepo->update($account);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     */
    public function update(Request $request)
    {
        $this->update_account($request->get('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pay_account(Request $request)
    {
        $this->update_account($request->get('data'));
    }


    private function update_account($data)
    {
        if ($data && count($data) > 0) {
            $details = isset($data['invoice']['details']) && count($data['invoice']['details']) > 0 ? $data['invoice']['details'] : null;
            $exist = Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->get();
            if (count($exist) > 0) {
                $data['details'] = $details;
                $this->accountRepo->update($data);
            } else {
                $accounts = new Account();
                $accounts->account_id = $data['account_id'];
                $accounts->waiter = $data['waiter'];
                $accounts->client = $data['client'];
                $accounts->currency = Helper::object_json($data['currency']);
                $accounts->table = $data['table'];
                $accounts->status = $data['status'];
                $accounts->token_id = $data['token_id'];
                $accounts->date = $this->formatData->formatDate($data['created_at']);
                $accounts->restaurant_id = session()->get('restaurant.id');
                $accounts->details = Helper::object_json($details);
                $accounts->save();
            }
        }
    }
}
