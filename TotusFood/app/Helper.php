<?php

namespace App;

use DateTime;
use DateTimeZone;

class Helper
{
    public static function json_object($string)
    {
        return json_decode($string);
    }

    public static function array_object(array $array)
    {
        return json_decode(json_encode($array));
    }

    public static function object_array($object)
    {
        return json_decode(json_encode($object), true);
    }

    public static function object_json($object)
    {
        return json_encode(self::object_array($object));
    }

    public static function date_convert($date, $format = 'Y-m-d')
    {
        return date($format, strtotime(str_replace('/', '-', $date)));
    }

    public static function number($number, $decimals = 2, $dec_point = '.', $thousands_sep = '') {
        return number_format($number, $decimals, $dec_point, $thousands_sep);
    }

    public static function date($time_transform, $time_zone = 'UTC', $format_time = 'F j, Y, g:i a', $tz = 'UTC') {
        $time_gmt     = new DateTime($time_transform, new DateTimeZone($tz));
        $time_tz      = new DateTimeZone($time_zone);
        $time_current = $time_gmt->setTimezone($time_tz);

        return $time_current->format($format_time);
    }

    public static function timezone()
    {
        return [
            'Pacific/Kwajalein',
            'Pacific/Samoa',
            'Pacific/Honolulu',
            'America/Juneau',
            'America/Los_Angeles',
            'America/Denver',
            'America/Mexico_City',
            'America/New_York',
            'America/Caracas',
            'America/St_Johns',
            'America/Argentina/Buenos_Aires',
            'Atlantic/Azores',
            'Europe/London',
            'Europe/Paris',
            'Europe/Helsinki',
            'Europe/Moscow',
            'Asia/Tehran',
            'Asia/Baku',
            'Asia/Kabul',
            'Asia/Karachi',
            'Asia/Calcutta',
            'Asia/Colombo',
            'Asia/Bangkok',
            'Asia/Singapore',
            'Asia/Tokyo',
            'Australia/Darwin',
            'Pacific/Guam',
            'Asia/Magadan',
            'Asia/Kamchatka',
        ];

    }

    public static function validText($string)
    {
        $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' , '/'=>'/', '*' => '*', ':' => ':' );
        $string = strtr( $string, $unwanted_array );
        return $string;
    }

    public static function centerText($str, $size) {
        $length = strlen($str);
        $pad    = (int) ceil(($size - $length) / 2);
        $line = ' ';
        for ($i = 1; $i < $pad; $i++) {
            $line .= ' ';
        }
        return $line . $str;
    }

    public static function formatCurrency($number, $currency, $cant = null) {
        $total  = is_null($cant) ? $number : floatval($number * $cant);
        $number = Helper::number($total, $currency['decimals'], $currency['decimal_separator'], $currency['thousand_separator']);
        return $number;
    }
}