<?php

namespace App\Printer\Repo;

use App\Printer\Model\Printers;


class PrinterRepo
{
    public function store($data)
    {
        $printer = new Printers();
    }

    public function updatePrinters($data)
    {
        $printers = new Printers();
        $printer = Printers::find($data['id']);

        if ($printer->title != $data['title']) {

            $printers->where('id', $data['id'])->update(['title' => $data['title']]);
        }

        if ($printer->type != $data['type']) {

            $printers->where('id', $data['id'])->update(['type' => $data['type']]);
        }

        if ($printer->source != $data['source']) {

            $printers->where('id', $data['id'])->update(['source' => $data['source']]);
        }
        if ($printer->host != $data['host']) {

            $printers->where('id', $data['id'])->update(['host' => $data['host']]);
        }
        if ($printer->port != $data['port']) {

            $printers->where('id', $data['id'])->update(['port' => $data['port']]);
        }
    }

}