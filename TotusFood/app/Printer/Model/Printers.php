<?php

namespace App\Printer\Model;


use Illuminate\Database\Eloquent\Model;

class Printers extends Model
{
    protected $table = 'printers';

    protected $fillable = ['id', 'title', 'type', 'source', 'host', 'port', 'predetermined', 'restaurant_id', 'status'];


}