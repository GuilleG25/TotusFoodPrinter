<?php

namespace App\Petitions\Enums;

class PetitionsStatusEnum
{
    public static $pending = 1;
    public static $in_progress = 2;
    public static $preparing = 3;
    public static $to_deliver = 4;
    public static $delivered = 5;
    public static $cancelled = 6;
}