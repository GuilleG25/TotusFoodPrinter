<?php

namespace App\Petitions\Repo;

use App\Helper;
use App\Petitions\Model\Petitions;

class PetitionsRepo
{
    private $petitions;
    function __construct(Petitions $petitions)
    {
        $this->petitions = $petitions;
    }

    public function store($data)
    {
        $this->petitions->fill($data);
        $this->petitions->save();
    }

    public function update($data)
    {
        $petition = $this->petitions->where('petition_id', $data['petition_id'])->first();
        if ($data['status'] != $petition->status) {
            $this->petitions->where('petition_id', $data['petition_id'])->update(['status' => $data['status']]);
        }

    }






}
