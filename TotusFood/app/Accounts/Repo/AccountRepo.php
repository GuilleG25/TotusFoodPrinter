<?php

namespace App\Accounts\Repo;


use App\Accounts\Model\Account;
use App\Helper;
use App\Petitions\Repo\FormatData;

class AccountRepo
{
    public function update($data)
    {
        $accounts = new Account();
        $accounts = $accounts->where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->get();
        $account  = $accounts[0];
        $invoice  = isset($data['invoice']) ? $data['invoice'] : null;
        $invoice_ = ! is_null($invoice) ? $invoice : null;
        $details  = ! is_null($invoice_) && count($invoice_) > 0 ? Helper::object_json($invoice_['details']) : null;

        if($account->waiter != $data['waiter']) {
            $waiter = is_null($data['waiter']) ? '' : $data['waiter'];
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['waiter' => $waiter]);
        }

        if($account->client != $data['client']) {
            $client = is_null($data['client']) ? '' : $data['client'];
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['client' => $client]);
        }

        if($account->table != $data['table']) {
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['table' => $data['table']]);
        }

        if($account->status != $data['status']) {
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['status' => $data['status']]);
        }

        if($account->token_id != $data['token_id']) {
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['token_id' => $data['token_id']]);
        }

        $formatData = new FormatData();
        $date = $formatData->formatDate($data['created_at']);

        if($account->date != $date) {
            Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['date' => $date]);
        }

        Account::where('account_id', $data['account_id'])->where('restaurant_id', session()->get('restaurant.id'))->update(['details' => $details]);

    }

}