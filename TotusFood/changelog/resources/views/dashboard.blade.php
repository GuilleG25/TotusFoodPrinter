@extends('template')
@section('content')
<div class="flex-center position-ref full-height" id="app" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="content">
        <div id="exTab1" class="container">
            <div class="row menu">
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#1a" data-toggle="tab" id="home">
                        <img id="img-home" src="{{ asset('images/home.svg') }}" alt="" width="32">
                        <br>
                        {{ __('Inicio') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#2a" data-toggle="tab" id="restaurant" v-on:click="showRestaurant()">
                        <img id="img-restaurant" src="{{ asset('images/restaurant-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Restaurant') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#3a" data-toggle="tab" id="printer" v-on:click="showPrinters()">
                        <img id="img-printer" src="{{ asset('images/printer-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Impresoras') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#4a" data-toggle="tab" id="about">
                        <img id="img-about" src="{{ asset('images/logo-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Acerca de') }}
                    </a>
                </div>
            </div>

            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a">
                    <div class="row" v-for="restaurant in restaurants">
                        <div class="col-xs-12">
                            <div class="col-xs-3">
                                <img v-if="restaurant.personalization.logo" class="logo-restaurant" v-bind:src="'{{ $settings->RETURN_IMAGE }}' + restaurant.token + '/logo/' + restaurant.personalization.logo" width="80" />
                                <img v-else="restaurant.personalization.logo" src="{{ asset('images/logo.svg') }}" alt="" width="80">
                            </div>
                            <div class="col-xs-7 text-center">
                                <h4>@{{ restaurant.name }}</h4>
                            </div>
                            <div class="col-xs-2">
                                <img v-bind:src="'{{ asset('images/flags') }}/' + restaurant.country + '.svg'" width="24" height="20" />
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-xs-12">
                        <div class="col-xs-6">
                            <a href="" class="btn btn-info btn-block" @click="goToWeb('{{ $settings->URL_BACK_GENERAL }}/dashboard/printers')"><i class="fa fa-cogs"></i> {{ __('Configurar') }}</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="" class="btn btn-info btn-block" @click="goToWeb('{{ $settings->HTTP }}' + domain + '.{{ $settings->DOMAIN }}')"><i class="fa fa-handshake-o"></i> {{ __('Aplicación Web') }}</a>
                        </div>
                    </div>
                    <br><br>
                    <span class="title">{{ __('Impresoras') }}:</span><br><br>
                    <div class="row" v-for="printer in printers">
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <img src="{{ asset('images/thermal-printer.svg') }}" alt="" width="32">
                            </div>
                            <div class="col-xs-8 text-uppercase">
                                <h4>@{{ printer.title }}</h4>
                            </div>
                            <div class="col-xs-2">
                                <i class="fa fa-toggle-on printer-active" v-if="printer.status == true"></i>
                                <i class="fa fa-toggle-on printer-inactive" v-else="printer.status == false"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="2a">
                  <div class="row" v-for="restaurant in restaurants">
                      <div class="col-xs-12">
                          <div class="col-xs-2 col-sm-2">
                              <img v-if="restaurant.personalization.logo" class="logo-restaurant" v-bind:src="'{{ $settings->RETURN_IMAGE }}' + restaurant.token + '/logo/' + restaurant.personalization.logo" width="115" />
                              <img v-else="restaurant.personalization.logo" src="{{ asset('images/logo.svg') }}" alt="" width="100">
                          </div>
                          <br><br>
                          <div class="col-xs-10 text-capitalize text-center">
                              <h1 class="title">@{{ restaurant.name }}</h1>
                          </div>
                          <br><br>
                          <div class="col-xs-12" v-if="restaurant.country">
                              <i class="fa fa-flag"></i>
                              <span class="title-dates-printer">{{ __('País') }}: </span>
                              <img v-bind:src="'{{ asset('images/flags') }}/' + restaurant.country + '.svg'" width="24" height="20"/>
                          </div>
                          <br><br>
                          <div class="col-xs-12" v-if="restaurant.subdomain">
                              <i class="fa fa-link"></i>
                              <span class="title-dates-printer">{{ __('Enlace') }}: </span>
                              <a>http://@{{ restaurant.subdomain }}.totusfood.com</a>
                          </div>
                          <br>
                          <div class="col-xs-12" v-if="restaurant.details.additional_information">
                              <i class="fa fa-map-marker" v-if="restaurant.details.additional_information.address_restaurant"></i>
                              <span class="title-dates-printer" v-if="restaurant.details.additional_information.address_restaurant"> {{ __('Dirección') }}: </span>
                              <span v-if="restaurant.details.additional_information.address_restaurant">@{{ restaurant.details.additional_information.address_restaurant }}</span>.
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="3a">
                    <h2 class="text-center">Impresoras</h2>
                    <div class="row"  v-for="printer in printers">
                        <div class="col-sm-12 printers">
                            <div class="col-xs-2">
                                <img src="{{ asset('images/thermal-printer.svg') }}" alt="" width="60">
                            </div>
                            <div class="col-xs-10 text-center title text-capitalize">@{{ printer.title }}
                                <i class="fa fa-toggle-on printer-active" v-if="printer.status == true"></i>
                                <i class="fa fa-toggle-on printer-inactive" v-else="printer.status == false"></i>
                            </div>
                            <div class="col-xs-9 dates-printer">
                                <div class="col-xs-12">
                                    <span class="title-dates-printer">{{ __('Tipo de conexión') }}:</span>
                                    <span v-if="printer.type == 1"> {{ __('Conexión Local') }}</span>
                                    <span v-if="printer.type == 2"> {{ __('Conexión Red') }}</span>
                                </div>

                                <div class="col-xs-12">
                                    <span v-if="printer.source" class="title-dates-printer">{{ __('Directorio') }}:</span>
                                    <span v-if="printer.source"> @{{ printer.source }}</span>
                                    <span v-if="printer.host"  class="title-dates-printer">{{ __('IP') }}:</span>
                                    <span v-if="printer.host"> @{{ printer.host }}</span>
                                </div>
                                <div class="col-xs-12" v-if="printer.port">
                                    <span class="title-dates-printer">{{ __('Puerto') }}:</span>
                                    <span v-if="printer.port"> @{{ printer.port }}</span>
                                </div>
                                <div class="col-xs-12">
                                    <span class="title-dates-printer">{{ __('Tamaño de papel') }}:</span>
                                    <span> @{{ printer.paper }}</span>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <a v-bind:id="'printer-connection-' + printer.id" class="btn btn-info btn-block btn-actions btn-connection" @click="testConnection(printer)" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Verificando conexión") }}..."><i class="fa fa-plug"></i> {{ __('Probar conexión') }}</a>
                            </div>
                            <div class="col-xs-6">
                                <a v-bind:id="'printer-' + printer.id" class="btn btn-success btn-block btn-actions btn-printer" @click="printerTest(printer)" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Imprimiendo") }}..."><i class="fa fa-print"></i> {{ __('Probar Impresión') }}</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="tab-pane" id="4a">
                    <div class="container-about">
                        <div class="col-xs-4 col-sm-4 text-center">
                            <img src="{{ asset('images/logo.svg') }}" alt="" width="80">
                        </div>
                        <div class="col-xs-8 col-sm-8 text-center">
                            <h3>{{ __('Totus Food Printer') }}</h3>
                            <span class="text-center">{{ __('Versión')}} {{ $version }}</span>
                        </div>
                        <div class="col-xs-12 about-content">
                            <p>
                                {{ __('Este software fué desarrollado por el departamento de tecnología de Totus Food Services, bajo la Licencia Pública General de GNU, para ser utilizado en los servicios Totus Food para el módulo de impresiones, se prohibe la distribución o comercialización total sin previa autorización de Totus Food Services') }}.
                            </p>
                            <p>
                                {{ __('Es un marca registrada de Totus Food Services, LLC') }}.
                            </p>
                            <span>{{ __('Sitio Web Oficial') }}:</span> <a href="#" @click="goToWeb('https://www.totusfood.com')">https://www.totusfood.com/</a>

                            <p><br>
                                {{ __('TotusFood producto de Totus Services LLC. Empresa registrada en los Estados Unidos bajo el número (EIN) 82-5434617') }}.<br>
                                {{ __('Dirección: 16979 SW 90TH TER, Miami, Florida 33196, EEUU') }}.<br>
                                {{ __('Números de teléfono') }}:<br>
                                {{ __('EEUU: +1(305)7674582') }}.<br>
                                {{ __('PERU: +51 917391967') }}.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    var app = new Vue({
        el: '#app',
        created: function () {
            this.showPrinters();
            this.showRestaurant();
        },
        data: {
            printers: [],
            restaurants: [],
            subdomain: null
        },
        methods: {
            showPrinters: function () {
                axios.post('{{ $settings->API_REST }}printers/all', {
                    id: '{{ session('restaurant.id') }}',
                    token: '{{ session('restaurant.token') }}'
                }).then(function (response) {
                    app.printers = response.data;
                }).catch(function (error) {
                    return error;
                });
            },
            showRestaurant: function () {
                axios.post('{{ $settings->API_REST }}printers/restaurant', {
                    id: '{{ session('restaurant.id') }}',
                    token: '{{ session('restaurant.token') }}'
                }).then(function (response) {
                    app.restaurants = response.data;
                    app.domain = response.data[0].subdomain;
                }).catch(function (error) {
                    return error;
                });
            },
            printerTest: function (printer) {
                $btn =  $("#printer-" + printer.id);
                $btn.button('loading');
                axios.post('{{ route('printer.test.local') }}', {
                    data: printer
                }).then(function (response) {
                    $btn.button('reset');
                    if (response.data.status === 200) {
                        toastrPersonalized.toastr(response.data.title, response.data.message, 'success');
                    }
                    else {
                        toastrPersonalized.toastr(response.data.title, response.data.message, 'error');
                    }
                }).catch(function (error) {
                    return error;
                });
            },
            testConnection: function (printer) {
                $btn =  $("#printer-connection-" + printer.id);
                $btn.button('loading');
                axios.post('{{ route('printer.test.connection') }}', {
                    data: printer
                }).then(function (response) {
                    $btn.button('reset');
                    if (response.data.status === 200) {
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: "success",
                            button: "{{ __("Aceptar") }}",
                            dangerMode: false
                        });
                    }
                    else {
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: "error",
                            button: "{{ __("Aceptar") }}",
                            dangerMode: true
                        });
                    }
                }).catch(function (error) {
                    return error;
                });
            },
            goToWeb: function (url) {
                axios.post('{{ route('printer.gotoweb') }}', {
                    url: url
                }).then(function (response) {
                    return false;
                }).catch(function (error) {
                    return error;
                });
            }
        },
        filters: {
            lowercase: function (value) {
                if (!value) return ''
                value = value.toString()
                return value.toLocaleLowerCase()
            }
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function () {
        var socket = io('{{ $settings->SERVER_SOCKET }}:{{ $settings->SERVER_SOCKET_PORT }}');
        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer', function(data){
            $.post("{{ url('printer') }}", {
                'data': data
            }, function (response) {
                if (response.status === 200) {
                    toastrPersonalized.toastr(response.title, response.message, 'success');
                } else {
                    toastrPersonalized.toastr(response.title, response.message, 'error');
                }
            });
        });

        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer-test', function(data){
            $.post("{{ url('printer/test') }}", {
                'data': data
            }, function (response) {
                if (response.status === 200) {
                    toastrPersonalized.toastr(response.title, response.message, 'success');
                } else {
                    toastrPersonalized.toastr(response.title, response.message, 'error');
                }
            });

            $.post("{{ url('printer/pong') }}", {
                'data': data
            }, function (response) {

            });
        });


        socket.on('TotusFood::server TotusFood printer-invoice-root', function(data){
            $.post("{{ route('printer.invoice.root') }}", {
                'data': data
            }, function (response) {

            })
        });

    });


    $(document).on('click', '#home', function () {

        $("#img-home").attr("src","{{ asset('images/home.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });

    $(document).on('click', '#restaurant', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });

    $(document).on('click', '#printer', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });


    $(document).on('click', '#about', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo.svg') }}");
    });

</script>

@stop

@section('scripts')

@stop






