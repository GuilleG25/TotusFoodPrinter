<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <title>Printer Desktop TotusFood</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/app.css?v=1') }}" rel="stylesheet" type="text/css"/>

    <link href="{{ asset('assets/toastr/toastr.min.css')}}" rel="stylesheet"/>
    <!-- Favicon and touch icons -->
{{--<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/totusfood.png')}}">--}}

<!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
          rel="stylesheet"/>
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{ asset('assets/toastr/toastr.min.css')}}" rel="stylesheet"/>
    <link href="{{ asset('css/styleBD.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet"/>
    <link href="{{ asset('assets/sweetalert2/sweetalert2.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/sweetalert2/sweetalert_custom.css')}}" rel="stylesheet">
    <link href="{{ asset('assets/parsley/parsley.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
    <script src="{{ asset('js/socket.io.js') }}"></script>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <!-- jQuery rest -->
    <script src="{{ asset('js/jquery.rest.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/toastr/toastr.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/toastrPersonalized.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/parsley/parsley.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/sweetalert2/sweetalert2.js')}}"></script>
    <script src="{{ asset('js/printer.js') }}"></script>
    <script src="{{ asset('js/env.js') }}"></script>
    <script src="{{ asset('assets/vue/vue.js') }}"></script>
    <script src="{{ asset('assets/axios/axios.min.js') }}"></script>
</head>
<body>
    <div>
        <div id="preload" style="display: none;">
            <div id="loader-wrapper">
                <div id="loader"></div>
            </div>
            <div id="loading">{{ __('Por favor espera...') }}</div>
        </div>
        <div id="update_notification" class="alert upgrade" style="display:none">
            <div class="row">
                <div class="col-xs-8 col-xs-8">
                    <span>{{ __("Actualización disponible") }}
                        <div class="badge badge-updater">v.{{ __('') }}</div></span>
                </div>
                <div class="col-sm-4 col-xs-4">
                    <div class="col-sm-6 col-xs-8">
                        <button class="btn" id="update"
                                data-loading-text="<i class='fa fa-spin fa-spinner'></i>{{ __("Actualizando") }}"><i
                                    class="fa fa-cloud-download"></i>{{ __("Actualizar") }}</button>

                    </div>
                    <div class="col-sm-6 col-xs-4">
                        <a type="button" style="margin-left: 20px; color:white" class="close" data-dismiss="alert"
                           aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->get('restaurant.id'))
            <div class="navbar-wrapper">
                <nav class="navbar navbar-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-8 col-sm-10">
                                <img src="{{ asset('images/logo.svg') }}" alt="" class="logo-bar"> <span
                                        class="restaurant-name"> {{ ucwords(session()->get('restaurant.name')) }}</span>
                            </div>
                            <div class="col-xs-4 col-sm-2 logout">
                                @if(Session::has('restaurant.token'))
                                    @if(Session::get('restaurant.token') === 'TotusFood')
                                        <a id="logout" data-url="{{ route('logout.root') }}" data-logout-api="{{ $settings->API_REST }}logout/app" data-token="{{ session('restaurant.token') }}" data-restaurant="{{ session('restaurant.id') }}">{{ __('Cerrar Sesión') }} <i
                                                    class="fa fa-power-off"></i></a>
                                    @else
                                        <a id="logout" data-url="{{ route('logout') }}" data-logout-api="{{ $settings->API_REST }}logout/app" data-token="{{ session('restaurant.token') }}" data-restaurant="{{ session('restaurant.id') }}">{{ __('Cerrar Sesión') }} <i
                                                    class="fa fa-power-off"></i></a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        @endif
        @yield('content')
        <input type="hidden" id="api" value="{{ $settings->API_REST }}"/>
        @if(session()->get('restaurant.id'))
            <footer class="footer">
                <div class="pull-right" id="version_app">v.{{ $version }}</div>
                <strong>Copyright &copy; 2016-{{date('Y')}} <a href="">TotusFood</a>.</strong>
            </footer>
        @endif
    </div>

</body>

@yield('scripts')
<script>
    $(function () {
        var socket = io('{{ $settings->SERVER_SOCKET }}:{{ $settings->SERVER_SOCKET_PORT }}');
        socket.on('TotusFood::server  update', function(data) {
            var $version_local = "{{ $version }}";
            if (data.version > $version_local) {
                $('#update_notification').show()
                $(".badge-updater").text(data.version)
            }
        });

        var settings = {
            titleLogout: "{{ __('¿Desea cerrar sesión?') }}",
            message: "{{ __('Debe asegurarse de no tener impresiones en cola, al cerrar este programa todas las impresiones son canceladas') }}.",
            accept: "{{ __('Aceptar') }}",
            cancel: "{{ __('Cancelar') }}",
            messasage_logout: "{{ __('¡Ha, cerrado sesión con éxitos!') }}",
            buttons: {
                confirmText: '{{ __('¡Si, cerrar sesión!') }}',
                cancelText: '{{ __('¡No, cancelar!') }}'
            }
        };

        Printer.init(settings);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).on('ready', function () {
        $.get("{{ $settings->URL_BACK_STORE }}versions.json", function (data) {
            var $version_local = "{{ $version }}";
            var $version_available = data.current;
            if ($version_available > $version_local) {
                $('#update_notification').show()
                $(".badge-updater").text($version_available)
            }
        });

        $(document).on('click', '#update', function () {
            btn = $(this);
            update(btn);
        });

        function update(btn) {
            btn.button('loading');
            $.get("{{ $settings->URL_BACK_STORE }}/versions.json", function (data) {
                current = data.current;
                versions = Object.values(data.versions);
                btn.button('loading');
                $.each(data.versions, function (i, value) {
                    btn.button('loading');
                    $.get('{{ route('printer.version') }}', function (version_local) {
                        if (value.version > version_local) {
                            btn.button('loading');
                            $.post('{{ __('update') }}', {
                                archive: value
                            }, function (response) {
                                btn.button('loading');
                                if (response.status === 200) {
                                    if (response.version === current) {
                                        $('#update_notification').hide()
                                        $('#version_app').text('v.' + response.version);
                                        swal({
                                            title: response.title,
                                            text: response.message,
                                            html: response.changelog.html,
                                            type: "success"
                                        });
                                        btn.button('reset');
                                    }
                                } else {
                                    swal({
                                        title: response.title,
                                        text: response.message,
                                        type: "error"
                                    });
                                    btn.button('reset');
                                }
                            });
                        }
                    });
                });
            });
        }

    });

</script>


</html>