<?php

namespace App\Printer\Template;


use App\Helper;
use Mike42\Escpos\Printer;
use Mike42\Escpos\CapabilityProfiles\DefaultCapabilityProfile;

class InvoiceRoot
{

    public $printer;
    public $char_per_line = 0;
    public $currency_symbol = "";
    public function printer($content, $connector, $paper)
    {
        try {
        $this->printer = new Printer($connector);
        $this->char_per_line = $paper;
        $title   =  __('Recibo de Pago');
        $order   = __("Orden");
        $Credits = "www.TotusFood.com";
        $textDate = Helper::validText(__("Fecha"));
        $textHour = Helper::validText(__("Hora"));
        $purshase = Helper::validText(__("Detalles de compra"));
        $devices = Helper::validText(__("Paquetes de dispositivos"));
        $modes = Helper::validText(__("Modos"));
        $modules = Helper::validText(__("Módulos"));
        $extras = Helper::validText(__("Servicios Extras"));
        $free = Helper::validText(__("Gratis"));
        $socialReasson = Helper::validText(__("Razón social"));
        $textTotal = Helper::validText(__("Total"));

        $this->printer->setTextSize(1, 2);
        $this->printer->text(Helper::centerText(Helper::validText(ucwords("{$title}")  . "\n \n"), $this->char_per_line));
        $this->printer->setTextSize(1, 1);
        $this->printer->selectPrintMode();
		$this->printer->setJustification();
        $this->drawLine();
        $this->printer->text("{$socialReasson}: {$content->restaurant} \n");
        $this->printer->text("{$order}: #{$content->order} \n");
        $this->printer->text($this->printLine("{$textDate}:{$content->date}^{$textHour}:{$content->hour} \n"));
        $this->printer->setJustification();
        $this->printer->text($this->drawLine());
        $this->printer->text(Helper::centerText(Helper::validText(ucwords("{$purshase}")  . "\n"), $this->char_per_line));
        $this->printer->text($this->drawLine());
        $this->printer->text("{$devices} \n");
        $this->printer->text($this->drawLine());
        foreach ($content->devices as $device) {
            $this->printer->text($this->printLine("{$device->title} ({$device->units})^{$content->currency->symbol} {$device->price} \n", $this->char_per_line));
        }
        $this->printer->text($this->drawLine());
        $this->printer->text("{$modes} \n");
        $this->printer->text($this->drawLine());
        foreach ($content->modes as $mode) {
            $this->printer->text($this->printLine("{$mode->title}^{$content->currency->symbol} {$mode->price} \n", $this->char_per_line));
        }
        $this->printer->text($this->drawLine());
        $this->printer->text("{$modules} \n");
        $this->printer->text($this->drawLine());
        foreach ($content->modules as $module) {
            $this->printer->text($this->printLine("{$module->title}^{$content->currency->symbol} {$module->price} \n", $this->char_per_line));
        }
        $this->printer->text($this->drawLine());
        $this->printer->text("{$extras} \n");
        $this->printer->text($this->drawLine());
        foreach ($content->extras as $extra) {
            if($extra->price == "0.00") {
                $this->printer->text($this->printLine("{$extra->title} - {$extra->currency}^{$free} \n", $this->char_per_line));
            } else {
                $this->printer->text($this->printLine("{$extra->title} - {$extra->currency}^{$content->currency->symbol} {$extra->price} \n", $this->char_per_line));
            }
        }

      
        $this->printer->text($this->drawLine());
        $this->printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
        $this->printer->text(Helper::validText($this->printLine("{$textTotal}^{$content->currency->symbol} {$content->total}", $this->char_per_line) . "\n"));
        $this->printer->feed();
        $this->printer->selectPrintMode();
        $this->printer->text("\n \n");

        $footer_1 = "TotusFood Product by Totus Services L.L.C.\nCompany signed in the United States under the number (EIN) 82-5434617\nAddress: 16979 SW 90TH TER, Miami, Florida 33196, EEUU";
        $footer_4 = "Phone Numbers";

        $phone_1 = __('EEUU: +1(305)7674582');
        $phone_2 = __('PERU: +51 917391967');
        $this->printer->selectPrintMode(Printer::MODE_FONT_A);
        $this->printer->text($this->printLineJustify("{$footer_1}",  $this->char_per_line));
        $this->printer->selectPrintMode();
        $this->printer->text("{$footer_4} \n");
        $this->printer->text("{$phone_1}. \n");
        $this->printer->text("{$phone_2}. \n \n");
        $this->printer->text(Helper::centerText(Helper::validText("www.TotusFood.com" . "\n"), $this->char_per_line));
        $this->printer->cut();
        $this->printer->close();

            $response = [
                'title' => __('¡Listo!'),
                'status' => 200,
                'message' => __("Prueba de impresión se realizó correctamente") . ".",

            ];

       } catch (\Exception $exception) {
            $response = [
                'title' => __('¡Error!'),
                'status' => 500,
                'message' => __('Error imprimiendo, por favor verifica el estado de impresora, alimentación de energía, puertos de conexión e intente nuevamente') . "."
            ];
        }
        
        return $response;

    }

    public function open_drawer()
    {
        $this->printer->pulse();
        $this->printer->close();
    }

    function drawLine()
    {
        $new = '';
        for ($i = 1; $i < $this->char_per_line; $i++) {
            $new .= '-';
        }
        return $new . "\n";

    }

    function printLine($str, $size = NULL, $sep = "^", $space = NULL)
    {
        if (!$size) {
            $size = $this->char_per_line;
        }
        $size = $space ? $space : $size;
        $length = strlen($str);
        list($first, $second) = explode("^", $str, 2);
        $line = $first . ($sep == "^" ? $sep : '');
        for ($i = 1; $i < ($size - $length); $i++) {
            $line .= ' ';
        }
        $line .= ($sep != "^" ? $sep : '') . $second;
        $line = str_replace("^", " ", $line);
        return $line;
    }

    function printLineJustify($str, $size = NULL)
    {
        if (!$size) {
            $size = $this->char_per_line;
        }
        $string = chunk_split($str, $size - 1) ."\n";
        return $string;
    }



}