<?php

Route::group(['middleware' => [
    'cors',
],  'prefix' => 'account',], function () {


    Route::post('store', [
        'as' => 'account.store',
        'uses' => 'AccountController@store',
    ]);

    Route::post('update', [
        'as' => 'account.update',
        'uses' => 'AccountController@update',
    ]);

    Route::post('all', [
        'as' => 'account.all',
        'uses' => 'AccountController@all',
    ]);

    Route::post('account/pay', [
        'as' => 'account.pay',
        'uses' => 'AccountController@pay_account',
    ]);


    Route::get('reports/{status}/{restaurant}', [
        'as' => 'account.reports',
        'uses' => 'AccountController@reports',
    ]);

    Route::get('reports/account/{id}/{restaurant}', [
        'as' => 'report.account.',
        'uses' => 'AccountController@report_by_account',
    ]);

//    Route::get('reports', 'AccountController@reports')->name('accounts.pdf');

});