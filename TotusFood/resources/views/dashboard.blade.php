@extends('template')
@section('content')
<div class="flex-center position-ref full-height" id="app" xmlns:v-on="http://www.w3.org/1999/xhtml"
     xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="content">
        <div class="alert alert-danger" style="position: relative; top: 20px;" v-if="predetermined === false && count_printers > 0">
            <div class="row">
                <div class="col-md-2 col-xs-2">
                    <i class="fa fa-exclamation-triangle" style="font-size: 3em"></i>
                </div>
                <div class="col-md-10 col-xs-10">
                    <span>{{ __('No tienes al menos una impresora Predefinida para este cliente Desktop App, por favor predefinir una o más impresoras') }}.
                        </span><a href="#success" class="btn btn-xs btn-danger pull-right" data-toggle="modal" style="padding: 3px; background-color: #ffffff; color: #dd4b39;">Predefinir</a>
                </div>
                <div class="col-md-12 col-xs-12">
                    <span style="font-size: 0.85em; font-style: italic">({{ __('Para realizar impresiones es requerido una impresora') }}).</span>
                </div>
            </div>
        </div>
        <div id="exTab1" class="container">
            <div class="row menu">
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#1a" data-toggle="tab" id="home" v-on:click="allPrinter()">
                        <img id="img-home" src="{{ asset('images/home.svg') }}" alt="" width="32">
                        <br>
                        {{ __('Inicio') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#2a" data-toggle="tab" id="restaurant" v-on:click="allAccounts()">
                        <img id="img-restaurant" src="{{ asset('images/restaurant-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Cuentas') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#3a" data-toggle="tab" id="printer" v-on:click="showPrinters()">
                        <img id="img-printer" src="{{ asset('images/printer-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Impresoras') }}
                    </a>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3 text-center">
                    <a href="#4a" data-toggle="tab" id="about">
                        <img id="img-about" src="{{ asset('images/logo-disabled.svg') }}" alt="" width="34">
                        <br>
                        {{ __('Acerca de') }}
                    </a>
                </div>
            </div>

            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a" style="scroll: auto">
                    <div class="row" v-for="restaurant in restaurants">
                        <div class="col-xs-12">
                            <div class="col-xs-2 col-sm-2" v-cloak>
                                <img v-if="restaurant.personalization.logo" class="logo-restaurant" v-bind:src="'{{ $settings->RETURN_IMAGE }}' + restaurant.token + '/logo/' + restaurant.personalization.logo" width="115" />
                                <img v-else="restaurant.personalization.logo" src="{{ asset('images/logo.svg') }}" alt="" width="100">
                            </div>
                            <br><br>
                            <div class="col-xs-10 text-capitalize text-center" v-cloak>
                                <h1 class="title">@{{ restaurant.name }}</h1>
                            </div>
                            <br><br>
                            <div class="col-xs-12" v-if="restaurant.country" v-cloak>
                                <i class="fa fa-flag"></i>
                                <span class="title-dates-printer">{{ __('País') }}: </span>
                                <img v-bind:src="'{{ asset('images/flags') }}/' + restaurant.country + '.svg'" width="24" height="20"/>
                            </div>
                            <br><br>
                            <div class="col-xs-12" v-if="restaurant.subdomain" v-cloak>
                                <i class="fa fa-link"></i>
                                <span class="title-dates-printer">{{ __('Enlace') }}: </span>
                                <a>http://@{{ restaurant.subdomain }}.totusfood.com</a>
                            </div>
                            <br>
                            <div class="col-xs-12" v-if="restaurant.details.additional_information" v-cloak>
                                <i class="fa fa-map-marker" v-if="restaurant.details.additional_information.address_restaurant"></i>
                                <span class="title-dates-printer" v-if="restaurant.details.additional_information.address_restaurant"> {{ __('Dirección') }}: </span>
                                <span v-if="restaurant.details.additional_information.address_restaurant">@{{ restaurant.details.additional_information.address_restaurant }}</span>.
                            </div>
                        </div>
                    </div>
                    <br><br>
                    <div class="col-xs-12">
                        <div class="col-xs-6">
                            <a href="" class="btn btn-info btn-block" @click="goToWeb('{{ $settings->URL_BACK_GENERAL }}')"><i class="fa fa-cogs"></i> {{ __('Administrador') }}</a>
                        </div>
                        <div class="col-xs-6">
                            <a href="" class="btn btn-info btn-block" @click="goToWeb('{{ $settings->HTTP }}' + domain + '.{{ $settings->DOMAIN }}')"><i class="fa fa-external-link"></i> {{ __('Aplicación Web') }}</a>
                        </div>
                    </div>
                    <br><br>
                    <span class="title" v-if="purchased === true && predetermined === true && count_printers > 0">{{ __('Impresoras') }}:</span><br>
                    <div><span style="float: right; margin-right: 10px; font-weight: bold;"  v-if="purchased === true && predetermined === true && count_printers > 0">{{ __('Predefinida') }}</span></div>
                    <div class="row" v-for="printer in all_printers" v-if="purchased === true && predetermined === true  && count_printers > 0" v-cloak>
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <img src="{{ asset('images/thermal-printer.svg') }}" alt="" width="32">
                            </div>
                            <div class="col-xs-8 text-uppercase">
                                <h4>@{{ printer.title }}</h4>
                            </div>
                            <div class="col-xs-2">
                                <div class="pretty p-switch p-fill">
                                    <input type="checkbox" name="predetermined" @change="change_status(printer.id, printer.predetermined)" :checked="printer.predetermined === '1'"/>
                                    <div class="state p-success">
                                        <label></label>
                                    </div>
                                </div>
                                {{--<i class="fa fa-toggle-on printer-active" v-if="printer.status == true"></i>--}}
                                {{--<i class="fa fa-toggle-on printer-inactive" v-else="printer.status == false"></i>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="2a">
                  <div>
                      <div class="row accounts-filters">
                          <div class="col-xs-12">
                              <div class="col-xs-7">
                                  <select name="status" id="status" title="Estatus de cuenta" @change="onChangeStatus()" class="form-control" v-model="statusAccount">
                                      <option value="" selected>{{ __('Seleccione') }}</option>
                                      <option value="5">{{ __('Todos') }}</option>
                                      <option value="1">{{ __('Abiertas') }}</option>
                                      <option value="4">{{ __('Canceladas') }}</option>
                                      <option value="3">{{ __('Cerradas') }}</option>
                                      <option value="2">{{ __('Pagando') }}</option>
                                  </select>
                              </div>
                              <div class="col-xs-4 col-xs-offset-0">
                                  <a href="javascript:void(0)" class="btn btn-block btn-reports" @click="reportsAccounts('{{ url('account/reports') }}/' + statusAccount + '/' + restaurant_id)"><i class="fa fa-file"></i> {{ __('Ver Reporte') }}</a>
                              </div>
                          </div>
                      </div>
                      <div class="row all_accounts" v-if="all_accounts.length > 0" v-for="(account, index) in all_accounts">
                          <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
                              <div class="fancy-collapse-panel">
                                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                      <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="headingOne" >
                                              <div class="panel-title">
                                                  <a data-toggle="collapse" data-parent="#accordion" v-bind:href="'#collapse_' +  account.id" aria-expanded="true" v-bind:aria-controls="'collapse_' + account.id">
                                                      <div class="row">
                                                          <div class="col-sm-5 col-xs-5"  v-cloak>
                                                              <span><strong>{{ __('Cuenta') }}</strong>:</span> <span>#@{{ account.token_id }}</span>
                                                          </div>
                                                          <div class="col-sm-7 col-xs-7 text-right"  v-cloak>
                                                              <span><strong>{{ __('Mesa') }}</strong>:</span> <span>#@{{ account.table }}</span>
                                                          </div>
                                                          <div class="col-sm-9 col-xs-9"  v-cloak>
                                                              <span><strong>{{ __('Mesero') }}</strong>:</span> <span>@{{ account.waiter }}</span>
                                                          </div>
                                                          <div class="col-sm-2 col-xs-2"  v-cloak>
                                                              <span style="font-size: 13px; font-style: italic; margin-right: 5%">(@{{ account.status }})</span>
                                                          </div>
                                                          <div class="col-sm-12 col-xs-12"  v-cloak>
                                                              <span style="margin-right: 5%; padding-left: 8px !important;"><strong>{{ __('Fecha') }}:</strong> @{{ account.date }}</span>
                                                          </div>
                                                      </div>
                                                  </a>
                                              </div>
                                          </div>
                                          <div v-bind:id="'collapse_' + account.id" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                              <div class="panel-body">
                                                  <div class="row" v-if="account.products_cant > 0">
                                                      <div class="col-sm-12 col-xs-12">
                                                          <div class="col-sm-6 col-xs-6">
                                                              <div class="col-xs-12" style="padding:0 5px !important;">
                                                                  <span><strong>{{ __('Producto') }}</strong>:</span>
                                                              </div>
                                                          </div>
                                                          <div class="col-sm-3 col-xs-3" style="padding: 0 2px !important;">
                                                              <span><strong>{{ __('Estatus') }}</strong>:</span>
                                                          </div>
                                                          <div class="col-sm-3 col-xs-3 text-right" style="padding: 0 4px !important;">
                                                              <span><strong>{{ __('Monto') }}</strong>:</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="products" v-if="account.products_cant > 0" v-for="products in account.products">
                                                      <div class="row">
                                                          <div class="col-sm-12 col-xs-12" :class="[products.products.status_ === '5' ? 'petitionsis-delivered' : '']">
                                                              <div class="col-sm-6 col-xs-6">
                                                                  <div class="col-xs-12" style="padding:0 5px !important;">
                                                                   (@{{ products.cant }}) x  - <span style="font-size: 13px;">@{{ products.products.product }}</span>
                                                                  </div>
                                                              </div>
                                                              <div class="col-sm-3 col-xs-3" style="padding: 0 2px !important;">
                                                                 <span style="font-style: italic; font-size:13px;">@{{ products.products.status }}</span>
                                                              </div>
                                                              <div class="col-sm-3 col-xs-3 text-right" style="padding: 0 4px !important;">
                                                                  <span style="font-size: 13px;" v-if="products.products.cancelled == false" class="is-bold" :class="[products.products.status_ !== '5' ? 'is-disable' : 'is-delivered']">@{{ account.currency.symbol }}@{{ products.products.amount | total_price(products.cant, account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</span>
                                                                  <span style="font-size: 13px;" v-if="products.products.cancelled == true"><strike>@{{ account.currency.symbol }}@{{ products.products.amount | total_price(products.cant, account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</strike></span>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <hr v-if="account.products_cant > 0" style="border: 0; height: 0;border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
                                                  <div class="col-xs-12"  v-if="account.products_cant > 0" style="padding: 5px !important;">
                                                      <div class="sub_total">
                                                          <div class="col-xs-5">
                                                              <span style="font-size: 14px">{{ __('SubTotal') }}</span>
                                                          </div>
                                                          <div class="col-xs-7 text-right" style="padding: 0 !important; font-weight: bold">
                                                              <span style="font-size: 14px">@{{ account.currency.symbol }}@{{ account.sub_total }}</span>
                                                          </div>
                                                      </div>
                                                      <div v-if="account.details_exist == true">
                                                          <div class="percentage_specials" v-if="account.details.percentage_special.length > 0" style="padding: 0 !important;">
                                                              <div  class="col-xs-12" v-for="percentage_special in account.details.percentage_special" style="padding: 0 !important;">
                                                                  <div class="col-xs-6">
                                                                      <span style="font-size: 13px">@{{ percentage_special.name }} (@{{ percentage_special.operator }}@{{ percentage_special.percentage }}%)</span>
                                                                  </div>
                                                                  <div class="col-xs-6 text-right"  style="padding: 0 !important; font-weight: bold">
                                                                      <span style="font-size: 13px">@{{ account.currency.symbol }}@{{ percentage_special.amount  | format_number(account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</span>
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div class="col-xs-12 tip" v-if="account.details.tip" style="padding: 0 !important;">
                                                              <div class="col-xs-6">
                                                                  <span style="font-size: 13px">{{ __('Propina') }} (@{{ account.details.tip_percentage }}%)</span>
                                                              </div>
                                                              <div class="col-xs-6 text-right" style="padding: 0 !important; font-weight: bold">
                                                                  <span style="font-size: 13px">@{{ account.currency.symbol }}@{{ account.details.tip | format_number(account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</span>
                                                              </div>
                                                          </div>

                                                          <div class="col-xs-12 tax" v-if="account.details.tax" style="padding: 0 !important; margin-bottom: 10px;">
                                                              <div class="col-xs-6">
                                                                  <span style="font-size: 13px">@{{ account.details.abbrev_tax }} (@{{ account.details.tax }}%)</span>
                                                              </div>
                                                              <div class="col-xs-6 text-right" style="padding: 0 !important; font-weight: bold">
                                                                  <span style="font-size: 13px">@{{ account.currency.symbol }}@{{ account.details.tax_amount | format_number(account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</span>
                                                              </div>
                                                          </div>
                                                          <hr style="margin-top: 1px" />
                                                          <div class="col-xs-12 total" v-if="account.details.amount_total" style="padding: 0 !important;">
                                                              <div class="col-xs-6">
                                                                  <span style="font-size: 18px">{{ __('Total') }}</span>
                                                              </div>
                                                              <div class="col-xs-6 text-right" style="padding: 0 !important; font-weight: bold">
                                                                  <span style="font-size: 18px">@{{ account.currency.symbol }}@{{ account.details.amount_total  | format_number(account.currency.decimals, account.currency.decimal_separator, account.currency.thousand_separator) }}</span>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-xs-12" style="padding: 0 0 0 15px !important; margin-top: 15px; font-weight: 700">
                                                          <a class="btn btn-block btn-details"  href="javascript:void(0)" @click="reportsByAccounts('{{ url('account/reports/account') }}/' + account.account_id + '/' + restaurant_id)"><i class="fa fa-eye"></i> {{ __('Ver detalles') }}</a>
                                                      </div>
                                                  </div>
                                                  <div class="products" v-if="account.products_cant == 0">
                                                      <div class="row">
                                                          <div class="col-sm-12 col-xs-12 text-center">
                                                              <h4><i class="fa fa-ban"></i> {{ __('No hay ordenes') }}</h4>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="products" v-if="all_accounts.length == 0">
                          <div class="row">
                              <div class="col-sm-12 col-xs-12 text-center">
                                  <h3><i class="fa fa-ban"></i> @{{ statusAccountTitle }}</h3>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="tab-pane" id="3a">
                    <h2 class="text-center" v-if="purchased === true">Impresoras</h2>
                    <div class="row"  v-for="printer in printers" v-if="purchased === true">
                        <div class="col-sm-12 printers">
                            <div class="col-xs-2">
                                <img src="{{ asset('images/thermal-printer.svg') }}" alt="" width="60">
                            </div>
                            <div class="col-xs-10 text-center title text-capitalize">@{{ printer.title }}
                                <i class="fa fa-check-circle printer-active" v-if="printer.status == true" title="{{ __('Impresora activa')}}"></i>
                                <i class="fa fa-check-circle printer-inactive" v-else="printer.status == false" title="{{ __('Impresora inactiva')}}"></i>
                            </div>
                            <div class="col-xs-9 dates-printer">
                                <div class="col-xs-12">
                                    <span class="title-dates-printer">{{ __('Tipo de conexión') }}:</span>
                                    <span v-if="printer.type == 1"> {{ __('Conexión Local') }}</span>
                                    <span v-if="printer.type == 2"> {{ __('Conexión Red') }}</span>
                                </div>

                                <div class="col-xs-12">
                                    <span v-if="printer.source" class="title-dates-printer">{{ __('Directorio') }}:</span>
                                    <span v-if="printer.source"> @{{ printer.source }}</span>
                                    <span v-if="printer.host"  class="title-dates-printer">{{ __('IP') }}:</span>
                                    <span v-if="printer.host"> @{{ printer.host }}</span>
                                </div>
                                <div class="col-xs-12" v-if="printer.port">
                                    <span class="title-dates-printer">{{ __('Puerto') }}:</span>
                                    <span v-if="printer.port"> @{{ printer.port }}</span>
                                </div>
                                <div class="col-xs-12">
                                    <span class="title-dates-printer">{{ __('Tamaño de papel') }}:</span>
                                    <span> @{{ printer.paper }}</span>
                                </div>
                            </div>

                            <div class="col-xs-6">
                                <a v-bind:id="'printer-connection-' + printer.id" class="btn btn-info btn-block btn-actions btn-connection" @click="testConnection(printer)" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Verificando conexión") }}..."><i class="fa fa-plug"></i> {{ __('Probar conexión') }}</a>
                            </div>
                            <div class="col-xs-6">
                                <a v-bind:id="'printer-' + printer.id" class="btn btn-success btn-block btn-actions btn-printer" @click="printerTest(printer)" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Imprimiendo") }}..."><i class="fa fa-print"></i> {{ __('Probar Impresión') }}</a>
                            </div>
                        </div>
                    </div>
                    <div v-if="count_printers === 0" style="text-align: center; font-size: 20px; margin-top: 10%;">
                        <i class="fa fa-print"></i> <span class="text-center">{{ __('No tienes impresoras registradas')}}</span>
                    </div>
                    <div class="purchased" v-if="purchased === false">
                       <div class="purchase">
                           <h3 class="text-center">{{ __('Módulo no disponible') }}</h3>
                           <p>
                               {{ __('Mejora tu experiencia Totus Food Printer.') }}
                               {{ __('Verifica el estatus de tu módulo de impresiones. Si aún no tienes el servicio,  puedes contactar nuestro soporte para brindarte mayor información.') }}
                           </p>
                           <div class="contact_us">
                               {{--{{ __('Contáctanos haciendo click en') }}<br>--}}
                               <a class="contact_us" @click="goToWeb('https://www.totusfood.com')">{{ __('Contáctanos Ahora') }}</a>
                           </div>
                       </div>

                        <img class="lock-purchased" src="{{ asset('images/lock_yellow.svg') }}" alt="" width="200">
                    </div>
                </div>
                <div class="tab-pane" id="4a">
                    <div class="container-about">
                        <div class="col-xs-4 col-sm-4 text-center">
                            <img src="{{ asset('images/logo.svg') }}" alt="" width="80">
                        </div>
                        <div class="col-xs-8 col-sm-8 text-center">
                            <h3>{{ __('Totus Food Desktop') }}</h3>
                            <span class="text-center">{{ __('Versión')}} {{ $version }}</span>
                        </div>
                        <div class="col-xs-12 about-content">
                            <p>
                                {{ __('Este software fué desarrollado por el departamento de tecnología de Totus Food Services, bajo la Licencia Pública General de GNU, para ser utilizado en los servicios Totus Food para el módulo de impresiones y ordenes offline, se prohibe la distribución o comercialización total sin previa autorización de Totus Food Services') }}.
                            </p>
                            <p>
                                {{ __('Es un marca registrada de Totus Food Services, LLC') }}.
                            </p>
                            <span>{{ __('Sitio Web Oficial') }}:</span> <a href="#" @click="goToWeb('https://www.totusfood.com')">https://www.totusfood.com/</a>

                            <p><br>
                                {{ __('TotusFood producto de Totus Services LLC. Empresa registrada en los Estados Unidos bajo el número (EIN) 82-5434617') }}.<br>
                                {{ __('Dirección: 16979 SW 90TH TER, Miami, Florida 33196, EEUU') }}.<br>
                                {{ __('Números de teléfono') }}:<br>
                                {{ __('EEUU: +1(305)7674582') }}.<br>
                                {{ __('PERU: +51 917391967') }}.
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 999999">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <h1 style="font-size: 25px;"><i class="glyphicon glyphicon-print"></i>  {{ __('Predefinir impresoras') }}</h1>
                </div>
                <div class="modal-body">
                    <div style="font-weight: bold; margin-bottom: 10px">
                        <span>{{ __('Impresoras') }}</span> 
                        <span style="float: right;">{{ __('Predefinida') }}</span>
                    </div>
                    <div class="row" v-for="printer in all_printers" v-if="purchased === true" v-cloak>
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <img src="{{ asset('images/thermal-printer.svg') }}" alt="" width="32">
                            </div>
                            <div class="col-xs-8 text-uppercase">
                                <h4>@{{ printer.title }}</h4>
                            </div>
                            <div class="col-xs-2">
                                <div class="pretty p-switch p-fill">
                                    <input type="checkbox" name="predetermined" @change="change_status(printer.id, printer.predetermined)" :checked="printer.predetermined === '1'"/>
                                    <div class="state p-success">
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-ban"></i> {{ __('Cerrar') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


</div>

<script>

    var app = new Vue({
        el: '#app',
        data: {
            restaurant_id: '{{ session('restaurant.id') }}',
            printers: [],
            restaurants: [],
            subdomain: null,
            purchased: false,
            all_printers: [],
            all_accounts: [],
            statusAccount: '5',
            statusAccountTitle: '',
            state: false,
            predetermined: false,
            count_printers: 0
        },
        methods: {
            showPrinters: function () {
                var purchased_app = "{{ session('restaurant.purchased') }}";
                if (purchased_app === 'true') {
                    axios.post('{{ $settings->API_REST }}printers/all', {
                        id: '{{ session('restaurant.id') }}',
                        token: '{{ session('restaurant.token') }}',
                        remember: '{{ $restaurant->remember }}'
                    }).then(function (response) {
                        app.printers = response.data;
                        app.purchased = true;
                    }).catch(function (error) {
                        return error;
                    });
                }

            },
            allPrinter: function () {
                axios.get('{{ route('printer.all') }}')
                    .then(function (response) {
                        app.all_printers = response.data;
                        app.count_printers = response.data.length;
                        console.log(response.data.length, app.count_printers);
                    }).catch(function (error) {
                    return error;
                });
            },
            showRestaurant: function () {
                var purchased_app = "{{ session('restaurant.purchased') }}";
                axios.post('{{ $settings->API_REST }}printers/restaurant', {
                    id: '{{ session('restaurant.id') }}',
                    token: '{{ session('restaurant.token') }}',
                    remember: '{{ $restaurant->remember }}'
                }).then(function (response) {
                    app.restaurants = response.data;
                    app.domain = response.data[0].subdomain;
                }).catch(function (error) {
                    return error;
                });

                if(purchased_app === 'true') {
                    app.purchased = true;
                }
            },
            printerTest: function (printer) {
                $btn =  $("#printer-" + printer.id);
                $btn.button('loading');
                axios.post('{{ route('printer.test.local') }}', {
                    data: printer
                }).then(function (response) {
                    $btn.button('reset');
                    if (response.data.status === 200) {
                        toastrPersonalized.toastr(response.data.title, response.data.message, 'success');
                    }
                    else {
                        toastrPersonalized.toastr(response.data.title, response.data.message, 'error');
                    }
                }).catch(function (error) {
                    return error;
                });
            },
            testConnection: function (printer) {
                $btn =  $("#printer-connection-" + printer.id);
                $btn.button('loading');
                axios.post('{{ route('printer.test.connection') }}', {
                    data: printer
                }).then(function (response) {
                    $btn.button('reset');
                    if (response.data.status === 200) {
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: "success",
                            button: "{{ __("Aceptar") }}",
                            dangerMode: false
                        });
                    }
                    else {
                        swal({
                            title: response.data.title,
                            text: response.data.message,
                            type: "error",
                            button: "{{ __("Aceptar") }}",
                            dangerMode: true
                        });
                    }
                }).catch(function (error) {
                    return error;
                });
            },
            goToWeb: function (url) {
                axios.post('{{ route('printer.gotoweb') }}', {
                    url: url
                }).then(function (response) {
                    return false;
                }).catch(function (error) {
                    return error;
                });
            },
            storePrinters: function () {
                var purchased_app = "{{ session('restaurant.purchased') }}";
                if(purchased_app === 'true') {
                    axios.post('{{ $settings->API_REST }}printers/all', {
                        id: '{{ session('restaurant.id') }}',
                        token: '{{ session('restaurant.token') }}',
                        remember: '{{ $restaurant->remember }}'
                    }).then(function (response) {
                        axios.post('{{ route('printer.store') }}', {
                            data: response.data
                        }).then(function (resp) {
                            app.allPrinter();
                            app.showPrinters();
                        });
                    }).catch(function (error) {
                        return error;
                    });
                }
            },
            updatePrinters: function () {
                app.storePrinters();
            },
            deletePrinters: function (id) {
                axios.post('{{ route('printer.delete') }}', {
                    id: id
                }).then(function (response) {
                        app.allPrinter();
                        app.showPrinters();
                    return false;
                }).catch(function (error) {
                    return error;
                });
            },
            deletePrinterTrash: function () {
                axios.post('{{ $settings->API_REST }}printers/all/trash', {
                    id: '{{ session('restaurant.id') }}',
                    token: '{{ session('restaurant.token') }}',
                    remember: '{{ $restaurant->remember }}'
                }).then(function (response) {
                    axios.post('{{ route('printer.trash') }}', {
                        data: response.data
                    }).then(function (resp) {
                        app.allPrinter();
                    });
                }).catch(function (error) {
                    return error;
                });
            },
            storeAccount: function (data) {
                axios.post('{{ route('account.store') }}', {
                    data: data
                }).then(function (response) {
                    app.allAccounts();
                    return false;
                })
                .catch(function (err) {
                    console.log(err)
                })
            },
            updateAccount: function (data) {
                axios.post('{{ route('account.update') }}', {
                    data: data
                }).then(function (response) {
                    app.allAccounts();
                    return false;
                }).catch(function (err) {
                    console.log(err)
                })
            },
            storePetitions: function (data) {
                axios.post('{{ route('petitions.store') }}', {
                    data: data
                }).then(function (response) {
                    app.allAccounts();
                    return false;
                }).catch(function (err) {
                    console.log(err)
                })
            },
            allAccounts: function () {
                axios.post('{{ route('account.all') }}', {
                    status: this.statusAccount
                }).then(function (response) {
                        app.all_accounts = response.data.data;
                        return false;
                    }).catch(function (err) {
                    console.log(err)
                })
            },
            payAccount: function (data) {
                axios.post('{{ route('account.pay') }}', {
                    data: data
                }).then(function (response) {
                    app.allAccounts();
                    return false;
                }).catch(function (err) {
                    console.log(err)
                })
            },
            changeAccountApp: function (data) {
                axios.post('{{ route('petitions.change') }}', {
                    data: data
                }).then(function (response) {
                    app.allAccounts();
                    return false;
                }).catch(function (err) {
                    console.log(err)
                })
            },
            reportsAccounts: function (url) {
                app.goToWeb(url);
            },
            reportsByAccounts: function (url) {
                app.goToWeb(url);
            },
            onChangeStatus: function () {
                app.allAccounts();
                switch (this.statusAccount) {
                    case '1':
                        this.statusAccountTitle = '{{ __('No hay Cuentas Abiertas') }}';
                        break;
                    case '2':
                        this.statusAccountTitle = '{{ __('No hay Cuentas Pagando') }}';
                        break;
                    case '3':
                        this.statusAccountTitle = '{{ __('No hay Cuentas Cerradas') }}';
                        break;
                    case '4':
                        this.statusAccountTitle = '{{ __('No hay Cuentas Canceladas') }}';
                        break;
                    default:
                        this.statusAccountTitle = '{{ __('Debes seleccionar un tipo de cuenta') }}';
                        this.all_accounts = [];
                        break;
                }
            },
            number_format: function (number,decimal,decimal_sep, thousand_sep) {
                number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
                var n = !isFinite(+number) ? 0 : +number,
                    prec = !isFinite(+decimal) ? 0 : Math.abs(decimal),
                    sep = (typeof thousand_sep === 'undefined') ? ',' : thousand_sep,
                    dec = (typeof decimal_sep === 'undefined') ? '.' : decimal_sep,
                    s = '',
                    toFixedFix = function (n, prec) {
                        var k = Math.pow(10, prec);
                        return '' + Math.round(n * k) / k;
                    };
                s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
                if (s[0].length > 3) {
                    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
                }
                if ((s[1] || '').length < prec) {
                    s[1] = s[1] || '';
                    s[1] += new Array(prec - s[1].length + 1).join('0');
                }
                return s.join(dec);
            },
             change_status: function (id, status) {
                 if (status == 0) {
                     app.state = true;
                 } else {
                    app.state = false;
                 }

                axios.post('{{ route('printer.update') }}', {
                    predetermined: app.state,
                    id: id
                }).then(function (response) {
                    // app.all_printers = response.data.data; 
                    app.allPrinter();  
                    app.printerPredetermined();
                }).catch(function (err) {
                console.log(err);
                });
            },
            checked: function () {
                return true;
            },
            printerPredetermined: function () {
                axios.get('{{ route('printer.predetermined') }}')
                    .then(function (response) {
                        app.predetermined = response.data.predetermined;
                        return false;
                    }).catch(function (error) {
                    return error;
                });
            }
        },
        filters: {
            lowercase: function (value) {
                if (!value) return '';
                value = value.toString();
                return value.toLocaleLowerCase()
            },
            total_price: function (value, cant,decimal, decimal_sep, thousand_sep) {
                var total = parseFloat(value * cant).toFixed(2);
                total = app.number_format(total, decimal, decimal_sep, thousand_sep)
                return total;
            },
            format_number: function (value, decimal,decimal_sep, thousand_sep) {
               number = app.number_format(value,decimal,decimal_sep, thousand_sep);
               return number;
            }
        }
    });

    app.deletePrinterTrash();
    app.showPrinters();
    app.showRestaurant();
    app.storePrinters();
    app.allAccounts();
    app.printerPredetermined();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(function () {

        var purchased = "{{ session()->get('restaurant.purchased') }}";

        var socket = io('{{ $settings->SERVER_SOCKET }}:{{ $settings->SERVER_SOCKET_PORT }}');
        // Petitions

        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} set_room_test', function(data){
           console.log(data)
        });

        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} new_account_app', function(data){
            if(data){
                app.storeAccount(data.account);
            }
        });

        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} closed_account_app', function(data){
            if(data){
                app.updateAccount(data.account);
                app.storePetitions(data.petitions)
            }
        });


        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} pay_account_app', function(data){
            if(data){
                app.updateAccount(data.account);
                app.payAccount(data.account);
                app.storePetitions(data.petitions)
            }
        });


        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} send_petition_app', function(data){
            if(data){
                app.updateAccount(data.account);
                app.storePetitions(data.petitions);
            }
        });


        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} update_account_app', function(data){
            if(data){
                app.updateAccount(data.account);
                app.storePetitions(data.petitions);
            }
        });


        socket.on('TotusFood::server {{ session()->get('restaurant.token') }} change_account_app', function(data){
            if(data){
                app.updateAccount(data.account);
                app.changeAccountApp(data.petitions);
            }
        });

        // Printers Module
        if(purchased === 'true') {
            socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer', function(data){
                $.post("{{ url('printer') }}", {
                    'data': data
                }, function (response) {
                    if (response.status === 200) {
                        toastrPersonalized.toastr(response.title, response.message, 'success');
                    } else {
                        toastrPersonalized.toastr(response.title, response.message, 'error');
                    }
                });
            });

            socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer-test', function(data){
                $.post("{{ url('printer/test') }}", {
                    'data': data
                }, function (response) {
                    if (response.status === 200) {
                        toastrPersonalized.toastr(response.title, response.message, 'success');
                    } else {
                        toastrPersonalized.toastr(response.title, response.message, 'error');
                    }
                });

                $.post("{{ url('printer/pong') }}", {
                    'data': data
                }, function (response) {

                });
            });

            socket.on('TotusFood::server TotusFood printer-invoice-root', function(data){
                $.post("{{ route('printer.invoice.root') }}", {
                    'data': data
                }, function (response) {

                })
            });

            socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer-store', function(data){
                if(data){
                    app.updatePrinters();
                }
            });

            socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer-update', function(data){
                if(data){
                    app.updatePrinters();
                }
            });

            socket.on('TotusFood::server {{ session()->get('restaurant.token') }} printer-delete', function(response){
                if(response.data){
                    app.deletePrinters(response.data.id);
                }
            });
        }
    });


    $(document).on('click', '#home', function () {

        $("#img-home").attr("src","{{ asset('images/home.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });

    $(document).on('click', '#restaurant', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });

    $(document).on('click', '#printer', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo-disabled.svg') }}");
    });


    $(document).on('click', '#about', function () {
        $("#img-home").attr("src","{{ asset('images/home-disabled.svg') }}");
        $("#img-printer").attr("src","{{ asset('images/printer-disabled.svg') }}");
        $("#img-restaurant").attr("src","{{ asset('images/restaurant-disabled.svg') }}");
        $("#img-about").attr("src","{{ asset('images/logo.svg') }}");
    });

</script>

@stop

@section('scripts')

@stop






