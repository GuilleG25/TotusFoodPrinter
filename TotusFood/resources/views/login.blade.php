@extends('template')

@section('content')
    <div class="logo-login">
        <img src="{{ asset('images/logo.svg') }}" alt="">
    </div>
    <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3" style="margin-top: 2%;">
        <div class="panel panel-bd login_view" style="margin-left: auto; margin-right: auto">
            <div class="panel-heading" style="padding-bottom: 25px;">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="material-icons" style="font-size: 50px;">lock_open</i>
                    </div>
                    <div class="header-title">
                        <small><strong>{{__('Por favor ingresa tus credenciales para acceder')}}.</strong></small>
                    </div>
                </div>
            </div>

            <div class="panel-body remember-panel">
                <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                    <h3 class="box-title m-b-20">{{$restaurant->name . __(', tienes una sesión activa')  }}.</h3>
                    <label for="remember">{{ __('¿Deseas continuar con esta sesión?') }}</label>
                    <input type="hidden" value="{{ $restaurant->id }}" id="restaurant-id">
                    <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 5px !important; padding-right: 5px !important;">
                        <button type="button" class="btn btn-success btn-block" id="continue" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Validando") }}...">
                            {{__('Si, continuar')}}
                        </button>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 5px !important; padding-right: 5px !important;">
                        <button type="button" class="btn btn-info btn-block" id="logout-remember" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Cerrando sesión") }}...">
                            {{__('No, cerrar sesión')}}
                        </button>
                    </div>

                </div>
            </div>

            <div class="panel-body remember-login">
                <form class="form-horizontal form-material" id="loginForm" data-url="{{ $settings->API_REST }}auth">
                    <h3 class="box-title m-b-20">{{__('Iniciar Sesión')}}</h3>
                    <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                        <div class="form-group ">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <input class="form-control" name="username" id="username" type="text" placeholder="{{__('Nombre de usuario')}}" data-parsley-required-message="{{ __("Nombre de usuario es requerido") }}." required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <input class="form-control" name="password" id="password" type="password" placeholder="{{__('Contraseña')}}" data-parsley-required-message="{{ __("Contraseña es requerido") }}." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <select class="form-control hidden" name="restaurant" id="restaurant" data-parsley-required-message="{{ __("Restaurant es requerido") }}."  data-restaurant="{{ $settings->API_REST }}restaurant">
                                    <option value="">{{ __('Seleccione restaurant') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-md-12">
                                <input type="hidden" value="">
                                <div class="col-sm-12 col-md-12">
                                    <label for="remember_check">{{ __('¿Deseas que Totus Food Printer mantenga tu sesión activa?') }}</label>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <div class="pretty p-switch p-fill">
                                        <input type="hidden" name="remember" id="remember" value=""/>
                                        <input type="checkbox" name="remember_check" id="remember_check"/>
                                        <div class="state p-success">
                                            <label>{{ __('Si, mantener sesión activa') }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12" style="margin-bottom: 10px;">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-success btn-block btn-login" id="login" data-loading-text="<i class='fa fa-spin fa-spinner'></i> {{ __("Iniciando sesión") }}...">
                                    <i class="fa fa-check"></i>{{__('Iniciar sesión')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
        <div class="credits">{{__('www.TotusFood.com')}}</div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function() {
            var lang = {
                messages: {
                    ready: '{{ __('¡Listo!') }}',
                    accept: '{{ __('Aceptar') }}',
                    failed:'{{ __('¡Ocurrió un error inesperado!') }}',
                    purchase:'{{ __('¡Adquiere este servicio!') }}',
                    purchaseText: '{{ __('Mejora tu experiencia Totus Food Printer, solicita el servicio, contacta nuestro soporte') }}.'
                }
            };

            Printer.auth(lang);
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var  remember =  "{{ $remember }}";
        if(remember === '1') {
            $('.remember-login').addClass('hidden');
        } else {
            $('.remember-panel').addClass('hidden');
        }

        $(document).on('click', '#logout-remember', function () {
            swal({
                title: '{{ __('¿Desea cerrar sesión?') }}',
                text: "{{ __('Todos los datos de sesión previamente almacenados serán borrados, debe ingresar las credenciales nuevamente') }}.",
                type: "warning",
                confirmButtonText: "{{ __('Si, cerrar esta sesión') }}",
                cancelButtonText: "{{ __('No, mantener esta sesión') }}",
                cancelButtonColor: '#b32828',
                reverseButtons: true,
                showCancelButton: true
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    btn = $(this);
                    btn.button('loading');
                    id = '{{ $restaurant->id }}'
                    $.post('{{ route('logout.remember') }}', {id: id}, function (response) {
                        if (response.status === 200) {
                            btn.button('reset');
                            $('.remember-login').removeClass('hidden');
                            $('.remember-panel').addClass('hidden');
                        }
                    })
                }
            });

        });

        $(document).on('click', '#continue', function () {
            btn = $(this);
            settings = {
                lang: {
                    messages: {
                        ready: '{{ __('¡Listo!') }}',
                        accept: '{{ __('Aceptar') }}',
                        failed:'{{ __('¡Ocurrió un error inesperado!') }}'
                    }
                },
                url: '{{ $settings->API_REST }}'
            };

            Printer.loginRemember(settings, btn);

        });


        $('#remember_check').on('change', function() {
            remember_check = $('input[name=remember_check]:checked', '#loginForm').val() === "on";
            remember       = $('#remember').val(remember_check);
        });

    </script>
@stop