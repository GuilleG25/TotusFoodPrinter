env = {
    roles: {
        super_admin: 1,
        owner: 2,
        restaurant_admin: 3,
        waiter: 4,
        pos: 5,
        kitchen: 6,
        kiosk: 8
    },
    modules: {
        impressions: 11
    },
    status: {
        ok: 200,
        accepted: 202,
        unauthorized: 401
    }
};