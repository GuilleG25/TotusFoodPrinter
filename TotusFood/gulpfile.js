
var gulp = require('gulp');
var sass = require('gulp-sass');

var scrFiles = "./resources/assets/sass/**/*.scss";
var destCss  = "public/css/*.css";
gulp.task('sass', function(){
    return gulp.src(scrFiles)
        .pipe(sass())
        .pipe(gulp.dest(destCss))
});

