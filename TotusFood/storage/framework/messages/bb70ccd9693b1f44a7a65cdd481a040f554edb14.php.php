<?php $__env->startSection('content'); ?>
        <div class="logo-login">
            <img src="<?php echo e(asset('img/logo.png')); ?>" alt="">
        </div>
    <div class="col-lg-6 col-md-6 col-lg-offset-3 col-md-offset-3" style="margin-top: 2%;">
        <div class="panel panel-bd login_view" style="margin-left: auto; margin-right: auto">
            <div class="panel-heading" style="padding-bottom: 25px;">
                <div class="view-header">
                    <div class="header-icon">
                        <i class="material-icons" style="font-size: 50px;">lock_open</i>
                    </div>
                    <div class="header-title">
                        <small><strong><?php echo e(__('Por favor ingresa tus credenciales para acceder')); ?>.</strong></small>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal form-material" id="loginForm" data-url="<?php echo e(env('API_REST')); ?>auth">
                    <h3 class="box-title m-b-20"><?php echo e(__('Iniciar Sesión')); ?></h3>
                    <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                        <div class="form-group ">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <input class="form-control" name="username" id="username" type="text" placeholder="<?php echo e(__('Nombre de usuario')); ?>" data-parsley-required-message="<?php echo e(__("Nombre de usuario es requerido")); ?>." required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <input class="form-control" name="password" id="password" type="password" placeholder="<?php echo e(__('Contraseña')); ?>" data-parsley-required-message="<?php echo e(__("Contraseña es requerido")); ?>." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                                <select class="form-control hidden" name="restaurant" id="restaurant" data-parsley-required-message="<?php echo e(__("Restaurant es requerido")); ?>."  data-restaurant="<?php echo e(env('API_REST')); ?>restaurant">
                                    <option value="0"><?php echo e(__('Seleccione restaurant')); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12" style="margin-bottom: 10px;">
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <button type="submit" class="btn btn-success btn-block btn-login" id="login" data-loading-text="<i class='fa fa-spin fa-spinner'></i> <?php echo e(__("Iniciando sesión")); ?>...">
                                    <i class="fa fa-check"></i><?php echo e(__('Iniciar sesión')); ?>

                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
        <div class="col-md-12 col-sm-12 col-ms-12 col-xs-12">
            <div class="credits"><?php echo e(__('www.TotusFood.com')); ?></div>
        </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $(function() {
            var lang = {
                messages: {
                    ready: '<?php echo e(__('¡Listo!')); ?>',
                    accept: '<?php echo e(__('Aceptar')); ?>',
                    failed:'<?php echo e(__('¡Ocurrió un error inesperado!')); ?>',
                    purchase:'<?php echo e(__('¡Adquiere este servicio!')); ?>',
                    purchaseText: '<?php echo e(__('Mejora tu experiencia Totus Food Printer, solicita el servicio, contacta nuestro soporte')); ?>.'
                }
            }


            Printer.auth(lang);
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>