<?php $__env->startSection('content'); ?>
<div class="flex-center position-ref full-height">
    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    <div class="content">
        <div class="title m-b-md">
            <img src="<?php echo e(asset('images/logo.png')); ?>" alt="" class="logo"><br>
             <span class="titles">TOTUS FOOD PRINTER DESKTOP APP</span>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            var socket = io('<?php echo e(env('SERVER_SOCKET')); ?>:<?php echo e(env('SERVER_SOCKET_PORT')); ?>');
            socket.on('TotusFood::server <?php echo e(session()->get('restaurant.name')); ?> printer', function(data){
                $.post("<?php echo e(url('printer')); ?>", {
                    'data': data
                }, function (response) {
                    if (response.status === 200) {
                        toastrPersonalized.toastr(response.title, response.message, 'success');
                    } else {
                        toastrPersonalized.toastr(response.title, response.message, 'error');
                    }
                    console.log(response);
                });

            });
        });

    </script>
<?php $__env->stopSection(); ?>







<?php echo $__env->make('template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>