<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Printer\PrinterESCPOS;
use Illuminate\Http\Request;


class PrintController extends Controller
{
    private $printerESCPOS;
    function __construct(PrinterESCPOS $printerESCPOS)
    {
        $this -> printerESCPOS = $printerESCPOS;
    }


    public function index()
    {
        if (session()->get('restaurant.id')) {
            return view('dashboard');
        } else {
            return view('login');
        }
    }


    public function dashboard(Request $request)
    {
        $data = $request->get('response');
        $request->session()->put('restaurant.id', $data['id']);
        $request->session()->put('restaurant.name', $data['name']);
        return [
            'url' => route('index'),
        ];
    }



    public function printer(Request $request)
    {
        $received = $request->get('data');
        $source   = $received['data']['printer'][0]['source'];
        $type     = $received['data']['printer'][0]['type'];
        $host     = $received['data']['printer'][0]['host'];
        $port     = $received['data']['printer'][0]['port'];
        $response = $this-> printerESCPOS->impression($received, $type, strtoupper($source), $host, $port, $received['data']['typePrint']);
        SocketIO::emit('TotusFood::server ' . session()->get('restaurant.name') . ' printer-response', ['data' => $response]);
       return $response;
    }


    public function printerTest()
    {
        $this -> printerESCPOS->TestPrinter('Page Test Printer Success', 'local', 'POS-58');
    }

    public function logout(Request $request)
    {
        $request->session()->forget('restaurant.id');
        $request->session()->forget('restaurant.name');
        return redirect('/');
    }


}


